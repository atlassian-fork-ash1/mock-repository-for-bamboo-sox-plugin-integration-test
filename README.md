# Mock repository for bamboo sox plugin integration test

This is a Git repository created for integation test of [bamboo-sox-plugin](https://stash.atlassian.com/projects/BUILDENG/repos/bamboo-sox-plugin/browse). 
It is made public so we do not need to put creds in the integration test.

`master` branch is configured to be sox compliant and `test` branch is not.